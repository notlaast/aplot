#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# TODO:
# - add support for atopreader
# - use CLI to control verbosity/debugging messages
# - add logging
#
"""
This module loads an interface that provides some nice widgets for
testing programs.

Depends on PyQt & pyqtgraph for the interface. The rest, as you please.
"""
# standard library
import sys
import os
from functools import partial
import pprint
import distutils.version
import datetime
import random
import fnmatch
import socket
# third-party
from pyqtgraph.Qt import QtGui, QtCore, QtSvg, QtWidgets
from pyqtgraph.Qt import uic
loadUiType = uic.loadUiType
import sip
import numpy as np
import pyqtgraph as pg
import pandas as pd
import pyLabSpec
from pyLabSpec import miscfunctions
from pyLabSpec.GUIs import (Widgets, Dialogs, DateAxisItem)
from pyLabSpec.Spectrum import plotlib
import pytz
from tzlocal import get_localzone
# local
if not os.path.dirname(os.path.realpath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import aplot

# fix compatibility issues
if sys.version_info[0] == 2:
	raise NotImplementedError("python 2.X is not supported!")
elif sys.version_info[0] == 3:
	from importlib import reload
	unicode = str
	xrange = range

if ((distutils.version.LooseVersion(pg.Qt.QtVersion) > "5") or
	sys.version_info[0] == 3):
	QtCore.QString = str

# misc. helpers
pp = pprint.PrettyPrinter(indent=4, compact=True)
tz = get_localzone()


# determine the correct containing the *.ui files
ui_filename = 'qtaplot.ui'
ui_path = ""
if os.path.isfile(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), ui_filename)):
	ui_path = os.path.dirname(os.path.realpath(sys.argv[0]))
elif os.path.isfile(os.path.join(os.path.dirname(__file__), ui_filename)):
	ui_path = os.path.dirname(__file__)
else:
	raise IOError("could not identify the *.ui files")

Ui_MainWindow, QDialog = loadUiType(os.path.join(ui_path, ui_filename))
class qtaplot(QDialog, Ui_MainWindow):
	"""
	Provides a GUI (as a QMainWindow)
	"""
	def __init__(self, debugging=False):
		"""
		Initializes the GUI.. standard stuff
		"""
		super(self.__class__, self).__init__()
		self.setupUi(self)
		self.debugging = debugging
		self.setWindowTitle("QtAPLOT (host: %s)" % (socket.gethostname(),))
		
		# button functionality
		self.btn_browseDir.clicked.connect(self.browseDir)
		self.btn_test.clicked.connect(self.test)
		self.btn_launch.clicked.connect(self.launch)
		self.btn_console.clicked.connect(self.showConsole)
		self.btn_quit.clicked.connect(self.quit)
		
		# gui elements/containers
		self.atopsarreader = aplot.AtopSarReader()
		# TODO: add support for original atopreader
		self.reports = []
		self.plot_data = []
		self.plot_tabs = {}
		self.plot_widgets = {}	# NOTE: these are PlotItems and not PlotWidgets
		
		# initializations
		self.init_metrics()
		self.init_text()
		self.init_table()
		self.updateFiles()

		# signals/slots
		self.shortcutSetDir = QtGui.QShortcut(QtGui.QKeySequence("Return"), self.txt_browseDir, activated=self.updateFiles)
		self.shortcutTest = QtGui.QShortcut(QtGui.QKeySequence("Ctrl+t"), self, activated=self.test)
		self.shortcutQuit = QtGui.QShortcut(QtGui.QKeySequence("Esc"), self, activated=partial(self.quit, confirm=True))
		self.shortcutQuit2 = QtGui.QShortcut(QtGui.QKeySequence("Ctrl+q"), self, activated=partial(self.quit, confirm=False))
	
	
	def init_metrics(self):
		"""
		initializes the metrics tab
		"""
		# TODO: set up input directory browser

		# set up atopsar options
		self.combo_atopsar.addItem("")
		for report in aplot.AtopSarReader._report_types.keys():
			self.combo_atopsar.addItem(report)
	
	def init_text(self):
		"""
		initializes the text tab
		"""
		font = QtGui.QFont()
		font.setFamily('Mono')
		self.textEdit.setFont(font)
	
	def init_table(self):
		"""
		initializes the table tab
		"""
		pass
	

	@QtCore.pyqtSlot()
	def browseDir(self, event=None):
		"""
		This is called when the 'browse' button is activated. Allows
		the selection of a directory containing atop log files.
		"""
		# determine the directory to show in the file dialog
		directory = os.getcwd()
		inserted_directory = os.path.dirname(str(self.txt_browseDir.text()))
		if os.path.isdir(inserted_directory):
			directory = os.path.realpath(inserted_directory)
		# get directory
		if distutils.version.LooseVersion(pg.Qt.QtVersion) > "5":
			path = QtWidgets.QFileDialog.getExistingDirectory(directory=directory)
		else:
			path = QtGui.QFileDialog.getExistingDirectory(directory=directory)
		if not str(path)[-1] == "/":
			path = "%s/" % (path,)
		print(path)
		self.txt_browseDir.setText(path)
		# update file listing
		self.updateFiles()
	
	@QtCore.pyqtSlot()
	def updateFiles(self, event=None):
		"""
		Updates the list of files with those within the directory
		defined by the text field above the list.
		"""
		# determine directory
		directory = os.getcwd()
		inserted_directory = os.path.dirname(str(self.txt_browseDir.text()))
		if os.path.isdir(inserted_directory):
			directory = os.path.realpath(inserted_directory)
		# clear list and add shortcuts
		self.list_files.clear()
		shortcuts = [
			("today", ""),
			("yesterday", "y"),
			("day before yesterday", "yy"),
			("3 days ago", "yyy"),
			("4 days ago", "yyyy"),
		]
		for shortcut in shortcuts:
			if distutils.version.LooseVersion(pg.Qt.QtVersion) > "5":
				i = QtWidgets.QListWidgetItem()
			else:
				i = QtGui.QListWidgetItem()
			i.setText(shortcut[0])
			i.fullpath = shortcut[1]
			self.list_files.addItem(i)
		# add files
		filenames = fnmatch.filter(os.listdir(directory), "atop_*")
		for file in sorted(filenames):
			if distutils.version.LooseVersion(pg.Qt.QtVersion) > "5":
				i = QtWidgets.QListWidgetItem()
			else:
				i = QtGui.QListWidgetItem()
			i.fullpath = os.path.join(directory, file)
			i.setText("%s" % file)
			self.list_files.addItem(i)


	@QtCore.pyqtSlot()
	def test(self):
		"""
		runs temporary tests (for debugging only)
		"""
		print("qtaplot.test() does nothing!")
	

	@QtCore.pyqtSlot()
	def launch(self, event=None):
		"""
		Launches reports based on the selected files, atopsar reports, and
		(optionally) specific metrics.
		"""
		# reset containers/widgets
		self.clear_plots()
		self.reports = []
		self.plot_data = []
		colormap = None
		# process inputs/parameters
		report_type = str(self.combo_atopsar.currentText())
		if not len(report_type):
			report_type = "CPU"
		files = [item.fullpath for item in self.list_files.selectedItems()]
		print(files)
		# files = ['y', 'yy']		# TODO: fix this
		isMultientry = aplot.AtopSarReader._report_types[report_type]['multientry']
		# loop through selected files
		for file in files:
			# get report from aplot.atopsarreader
			r = self.atopsarreader.get_report(
				report_type=report_type,
				file=file,
			)
			self.textEdit.append(
				"\n\n*******************\nfrom '%s' (%s):\n" % (file, report_type)
			)
			self.textEdit.append(
				"%s\n" % (pp.pformat(r),)
			)
			if (r['data'] == []) or (r['data'] == [{}]):
				print("warning: report was empty, skipping it")
				continue
			self.reports.append(r)
			# rearrange data to be plot-friendly
			if isMultientry:
				plots = {"timestamp":[]}
				# initialize lists for each group across all plots
				for i,c in enumerate(r['cols'][2:]):
					plots[c] = {}
					for g in r['groups']:
						plots[c][g] = np.empty(len(r['data']))
						plots[c][g][:] = np.NaN
						plots[c][g] = plots[c][g].tolist()
				# set data
				for i,entry in enumerate(r['data']):
					elapsedDays = 0
					prevTime = None
					for key,val in entry.items():
						if key == "timestamp":
							timestamp_str = "%s %s" % (r['date'], val)
							timestamp_dt = datetime.datetime.strptime(timestamp_str, '%Y/%m/%d %H:%M:%S')
							plots["timestamp"].append(timestamp_dt.replace(tzinfo=tz).timestamp())
							# track elapsed days
							if prevTime is not None:
								prevHour = prevTime.split(":")[0]
								thisHour = val.split(":")[0]
								if thisHour < prevHour:
									elapsedDays += 1
							prevTime = val
							plots["timestamp"][-1] += 86400*elapsedDays
							continue
						for j,c in enumerate(r['cols'][2:]):
							y = val[j]
							if y[-1] == "%":
								y = float(y[:-1]) * 0.01
							elif y[-1] == "M":
								y = float(y[:-1]) * 1e6
							try:
								plots[c][key][i] = float(y)
							except:
								pass
			else:
				plots = {"timestamp":[]}
				for c in r['cols'][1:]:
					plots[c] = []
				elapsedDays = 0
				prevTime = None
				for entry in r['data']:
					timestamp_str = "%s %s" % (r['date'], entry[0])
					timestamp_dt = datetime.datetime.strptime(timestamp_str, '%Y/%m/%d %H:%M:%S')
					plots["timestamp"].append(timestamp_dt.replace(tzinfo=tz).timestamp())
					# track elapsed days
					if prevTime is not None:
						prevHour = prevTime.split(":")[0]
						thisHour = entry[0].split(":")[0]
						if thisHour < prevHour:
							elapsedDays += 1
					prevTime = entry[0]
					plots["timestamp"][-1] += 86400*elapsedDays
					for i,y in enumerate(entry):
						if i == 0:
							continue
						c = r['cols'][i]
						if y[-1] == "%":
							y = float(y[:-1]) * 0.01
						elif y[-1] == "M":
							y = float(y[:-1]) * 1e6
						try:
							plots[c][key][i] = float(y)
						except:
							pass
			self.plot_data.append(plots)
		# sort reports & data by dates
		timestamps = [datetime.datetime.strptime(r['date'], '%Y/%m/%d').timestamp() for r in self.reports]
		timestamp_argsort = np.asarray(timestamps).argsort()
		self.reports = [x for _,x in sorted(zip(timestamps,self.reports))]
		self.plot_data = [x for _,x in sorted(zip(timestamps,self.plot_data))]
		# define colormaps for multientry reports (and across all files)
		if isMultientry:
			groups = []
			for r in self.reports:
				groups += r['groups']
			groups = sorted(list(set(groups)))
			palettes = ["cielab256", "coloralphabet26", "gilbertson", # misc palettes
						"nipy_spectral", "hsv", "Paired", "rainbow", # matplotlib palettes
						"thermal", "phase"] # cmocean palettes
			palette = random.sample(palettes, k=len(palettes))[0]
			newColors = plotlib.getColormap(
				num=len(groups)+1,
				palette=palette
			)
			colormap = {}
			for i,g in enumerate(groups):
				newColor = pg.mkColor(newColors[i])
				hsv = newColor.getHsv()
				newColor.setHsv(hsv[0], hsv[1], 255)
				colormap[g] = miscfunctions.qcolorToRGBA(newColor)
		# create new plot tabs/widgets
		for report_idx,plot_data in enumerate(self.plot_data):
			for plot_name,val in plot_data.items():
				if plot_name in ("timestamp",):
					continue
				if plot_name in self.plot_tabs:
					tab = self.plot_tabs[plot_name]
					plot = self.plot_widgets[plot_name]
				else:
					if distutils.version.LooseVersion(pg.Qt.QtVersion) > "5":
						tab = QtWidgets.QWidget()
						layout = QtWidgets.QHBoxLayout(tab)
					else:
						tab = QtGui.QWidget()
						layout = QtGui.QHBoxLayout(tab)
					tab.layout = layout
					self.tabWidget.addTab(tab, plot_name)
					self.plot_tabs[plot_name] = tab
					timeAxis = pg.DateAxisItem(orientation='bottom')
					plot_widget = pg.PlotWidget(
						tab,
						axisItems={'bottom': timeAxis}
					)
					layout.addWidget(plot_widget)
					plot = plot_widget.getPlotItem()
					labeltext = plot_name
					units = ''
					scale = 1
					### adjust labels/units/scale
					# fix memory
					if report_type in ("Memory", "Paging"):
						units = "Bytes"
					# fix inbound/outbound
					if ("Network" in report_type):
						if (plot_name[0] == "i"):
							labeltext = "inbound"
						elif (plot_name[0] == "o"):
							labeltext = "outbound"
						if "pack/s" in plot_name:
							units = "pack/s"
						elif "err/s" in plot_name:
							units = "err/s"
						elif "drop/s" in plot_name:
							units = "drop/s"
						elif "Kbyte/s" in plot_name:
							units = "Byte/s"
							scale = 1e3
						elif "mbps" in plot_name:
							units = "Byte/s"
							scale = 1e6
					plot.setLabel('left', labeltext, units=units, **{'color':'#FFF', 'font-size':'24pt'})
					plot.setLabel('bottom', "Timestamp")
					plot.getAxis('left').setScale(scale=scale)
					if isMultientry:
						legend = Widgets.LegendItem(offset=(30,30))
						legend.setParentItem(plot.vb)
						plot.legend = legend
					self.plot_widgets[plot_name] = plot
				# collect the curve(s) inside the widget and append the data
				if isMultientry:
					for entry_name,entry_data in val.items():
						# convert to floats
						try:
							entry_data = np.array(entry_data, dtype=float)
							if np.all(np.isnan(entry_data)):
								continue
						except:
							print("WARNING: could not convert data to np.array(dtype=float): %s" % (entry_data,))
						# collect curve or create new one
						curves = {}
						for c in plot.listDataItems():
							curves[c.name()] = c
						if entry_name in curves:
							curve = curves[entry_name]
						else:
							timeAxis = pg.DateAxisItem(orientation='bottom')
							curve = plot.plot(
								name=entry_name,
								axisItems={'bottom': timeAxis},
								# clipToView=True,	# NOTE: causes a problem with vb's autorange
								autoDownsample=True,
								downsampleMethod='subsample'
							)
							curve.setPen(pg.mkPen(colormap[entry_name]))
						# append the data
						x,y = curve.getData()
						try:
							x = x.tolist()
							y = y.tolist()
						except AttributeError:
							x = []
							y = []
						# clear zeros if empty plot
						if ((len(x) == 1) and (x[0] == 0.0) and
							(len(y) == 1) and (y[0] == 0.0)):
							x = []
							y = []
						timestamps = plot_data["timestamp"]
						if np.any(np.isnan(entry_data)):
							timestamps = np.asarray(timestamps)
							realvalues = np.logical_not(np.isnan(entry_data))
							timestamps = timestamps[ realvalues ].tolist()
							entry_data = entry_data[ realvalues ]
						x += timestamps
						y += entry_data.tolist()
						curve.setData(x, y)
				else:
					# convert to floats
					try:
						val = np.array(val, dtype=float)
						if np.all(np.isnan(val)):
							continue
					except:
						print("WARNING: could not convert data to np.array(dtype=float): %s" % (val,))
					# collect curve or create new one
					curves = {}
					for c in plot.listDataItems():
						curves[c.name()] = c
					if len(plot.listDataItems()):
						curve = plot.listDataItems()[0]
					else:
						# timeAxis = DateAxisItem.DateAxisItem(orientation='bottom')
						timeAxis = pg.DateAxisItem(orientation='bottom')
						curve = plot.plot(
							axisItems={'bottom': timeAxis},
							# clipToView=True,	# NOTE: causes a problem with vb's autorange
							autoDownsample=True,
							downsampleMethod='subsample'
						)
					# append the data
					x,y = curve.getData()
					try:
						x = x.tolist()
						y = y.tolist()
					except AttributeError:
						x = []
						y = []
					# clear zeros if empty plot
					if ((len(x) == 1) and (x[0] == 0.0) and
						(len(y) == 1) and (y[0] == 0.0)):
						x = []
						y = []
					timestamps = plot_data["timestamp"]
					if np.any(np.isnan(val)):
						timestamps = np.asarray(timestamps)
						realvalues = np.logical_not(np.isnan(val))
						timestamps = timestamps[ realvalues ].tolist()
						val = val[ realvalues ]
					x += timestamps
					y += val
					curve.setData(x, y)
	
	def clear_plots(self, event=None):
		"""
		Clears the plots and remove their tabs.

		NOTE: this is untested for large sets of plots! That is,
		it is uncertain whether the old items are actually removed
		from memory.
		"""
		for name,plot in self.plot_widgets.items():
			plot.clear()
		self.plot_widgets = {}
		for name,tab in self.plot_tabs.items():
			tab_idx = self.tabWidget.indexOf(tab)
			self.tabWidget.removeTab(tab_idx)
			sip.delete(tab)
		self.plot_tabs = {}


	@QtCore.pyqtSlot()
	def showConsole(self, event=None):
		"""
		Invoked when the 'Console' button is clicked. It provides a new
		window containing an interactive console that provides a direct
		interface to the current python instance, and adds the gui to
		that namespace for direct interactions.
		"""
		namespace = {'self': self}
		msg = "This is an active python console, where 'self' is the gui"
		msg += " object, whereby all its internal items/routines can thus"
		msg += " be made available for direct access."
		msg += "\n\nIf you want to enable additional print messages, run:"
		msg += "\n>self.debugging = True"
		msg += "\n\nEnjoy!"
		self.console = pg.dbg(namespace=namespace, text=msg)
		self.console.ui.catchAllExceptionsBtn.toggle()
		self.console.ui.onlyUncaughtCheck.setChecked(False)
		self.console.ui.runSelectedFrameCheck.setChecked(False)
		self.console.ui.exceptionBtn.toggle()
	
	
	@QtCore.pyqtSlot()
	def quit(self, confirm=False):
		"""
		Provides the routine that quits the GUI.
		
		For now, simply quits the running instance of Qt.
		
		:param confirm: (optional) whether to first use a confirmation dialog
		:type confirm: bool
		"""
		# first invoke the confirmation dialog if necessary
		if confirm:
			msg = "Are you sure you want to exit the program?"
			response = QtGui.QMessageBox.question(self, "Confirmation", msg,
				QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
			if response == QtGui.QMessageBox.No:
				return
		# finally quit
		QtCore.QCoreApplication.instance().quit()


if __name__ == '__main__':
	# monkey-patch exception hooks so they don't crash the GUI
	sys._excepthook = sys.excepthook
	def exception_hook(exctype, value, traceback):
		sys._excepthook(exctype, value, traceback)
	sys.excepthook = exception_hook

	# define GUI elements
	qApp = QtGui.QApplication(sys.argv)
	mainGUI = qtaplot()
	
	# start GUI
	mainGUI.show()
	qApp.exec_()
	qApp.deleteLater()
	sys.exit()
