#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This module provides a single source for details about the current version
of the package and some other related information.

Copyright (c) 2020 Jacob Laas <jclaas@gmail.com>
Distributed under the MIT license. See LICENSE for more infomation.
"""

__license__ = "MIT license"

__version__ = '1.0'
