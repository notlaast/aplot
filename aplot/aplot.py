#!/usr/bin/env python3
# coding: utf-8

"""
Atop log data analyzer.

See "{cmd} --help" for usage.
"""
# standard
from datetime import datetime, timedelta
from collections import OrderedDict
import re
import sys
import glob
import decimal
import subprocess
import argparse
from copy import deepcopy
# third-party
import iso8601
import humanfriendly
import pydash as py_


class AtopParser(object):

    _entry_regex = re.compile(r'^ATOP - (?P<HOST>\S+)\s*(?P<TIME>\d+/\d+/\d+\s+\d+:\d+:\d+)\s.*')
    _metric_regex = re.compile(r'^(?P<metric>(PRC|CPU|CPL|MEM|SWP|PAG|DSK|NET))\s(?P<details>.+)$')
    _field_regex = re.compile(r'\|\s+([^|\s]*)\s+([^|]+)\s+')

    _size_fields = [('MEM', 'buff'),
                    ('MEM', 'cache'),
                    ('MEM', 'free'),
                    ('MEM', 'slab'),
                    ('MEM', 'tot'),
                    ('SWP', 'free'),
                    ('SWP', 'tot'),
                    ('SWP', 'vmcom'),
                    ('SWP', 'vmlim'),
                    ('NET', 'si'),
                    ('NET', 'so')]

    _time_span_fields = [('DSK', 'avio'),
                         ('PRC', 'sys'),
                         ('PRC', 'user')]

    _percentage_fields = [('CPU', 'idle'),
                          ('CPU', 'irq'),
                          ('CPU', 'sys'),
                          ('CPU', 'user'),
                          ('CPU', 'wait'),
                          ('DSK', 'busy')]

    _float_fields = [('CPL', 'avg1'),
                     ('CPL', 'avg5'),
                     ('CPL', 'avg15')]

    _integer_fields = [('CPL', 'csw'),
                       ('CPL', 'intr'),
                       ('DSK', 'read'),
                       ('DSK', 'write'),
                       ('NET', 'pcki'),
                       ('NET', 'pcko'),
                       ('NET', 'si'),
                       ('NET', 'so'),
                       ('NET', 'deliv'),
                       ('NET', 'ipfrw'),
                       ('NET', 'ipi'),
                       ('NET', 'ipo'),
                       ('NET', 'tcpi'),
                       ('NET', 'tcpo'),
                       ('NET', 'udpi'),
                       ('NET', 'udpo'),
                       ('PAG', 'scan'),
                       ('PAG', 'stall'),
                       ('PAG', 'swin'),
                       ('PAG', 'swout'),
                       ('PRC', 'exit'),
                       ('PRC', 'proc'),
                       ('PRC', 'sys'),
                       ('PRC', 'user'),
                       ('PRC', 'zombie')]

    def __init__(self, min_date=None, max_date=None):
        self.min_date = min_date
        self.max_date = max_date
        self.current_time = None
        self.current_data = None
        self.result = OrderedDict()

    def _reset_current_entry(self):
        self.current_time = None
        self.current_data = None

    def _append_current_entry(self):
        if self.current_data is not None:
            self.result[self.current_time] = self.current_data
        self._reset_current_entry()

    def __enter__(self, ):
        self._reset_current_entry()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):

        if exc_type is None and exc_val is not None and exc_tb is not None:
            self._append_current_entry()

        return False

    @property
    def available_metrics_tuples(self):

        result = set()

        for entry in self.result.values():
            tmpentry = deepcopy(entry)
            py_.map_values_deep(tmpentry, lambda __, path: result.add(tuple(path)))

        return sorted(result)

    @property
    def available_metric_paths(self):

        return ['.'.join(p) for p in self.available_metrics_tuples]

    def _parse_field(self, field_metric, field_key, field_value):

        try:

            # sizes
            if (field_metric, field_key) in self._size_fields:
                return humanfriendly.parse_size(field_value)

            # time spans
            elif (field_metric, field_key) in self._time_span_fields:
                return sum(humanfriendly.parse_timespan(v)
                           if v else 0
                           for v in re.findall(r'([\d,\.]+\s*\D+)', field_value))

            # percentages
            elif (field_metric, field_key) in self._percentage_fields:
                return int(field_value.replace('%', '').strip())

            # floats
            elif (field_metric, field_key) in self._float_fields:
                return float(decimal.Decimal(field_value))

            # integers
            elif (field_metric, field_key) in self._integer_fields:
                return int(decimal.Decimal(field_value))

        except ValueError:
            pass

    def add_line(self, atop_line):

        match = self._entry_regex.match(atop_line)
        if match:
            # this is a new entry

            if self.current_data is not None:
                self._append_current_entry()

            entry_time = datetime.strptime(match.group('TIME'), '%Y/%m/%d %H:%M:%S')

            if self.min_date and entry_time < self.min_date:
                return False

            if self.max_date and entry_time > self.max_date:
                return False

            self.current_time = entry_time
            self.current_data = {}

            return True

        elif self.current_data is not None:
            # this is a new metric line

            match = self._metric_regex.match(atop_line)
            if match:

                line_metric = match.group('metric')
                line_details = match.group('details')
                metric_name = None
                row = {}

                for metric_key, metric_value in (m.groups() for m in self._field_regex.finditer(line_details)):

                    metric_key = metric_key.strip().replace('#', '') if metric_key.strip() else None
                    metric_value = metric_value.strip() if metric_value.strip() else None

                    if metric_key is None and metric_value is None:
                        continue

                    self.current_data[line_metric] = self.current_data.get(line_metric, {})

                    if line_metric in ('NET', 'DSK'):

                        if not metric_name:
                            if line_metric == 'NET':
                                metric_name = metric_key
                            elif line_metric == 'DSK' and metric_key is None:
                                metric_name = metric_value
                            self.current_data[line_metric][metric_name] = row
                        else:
                            parsed_value = self._parse_field(line_metric, metric_key, metric_value)
                            if parsed_value is not None:
                                row[metric_key] = parsed_value

                    else:
                        parsed_value = self._parse_field(line_metric, metric_key, metric_value)
                        if parsed_value is not None:
                            self.current_data[line_metric][metric_key] = parsed_value

                return True

        return False


class AtopSarReader(object):

    _report_types = {
        # TODO: finish this https://linux.die.net/man/1/atopsar
        # "Top-3 Pro. CPU": {"description":"Top-3 Pro. vs CPU", "flag":"-O", "multientry":True},
        # "Top-3 Pro. Mem": {"description":"Top-3 Pro. vs Mem", "flag":"-G", "multientry":True},
        # "Top-3 Pro. Disk": {"description":"Top-3 Pro. vs Disk", "flag":"-D", "multientry":True},
        # "Top-3 Pro. Net": {"description":"Top-3 Pro. vs Disk", "flag":"-N", "multientry":True},
        "CPU": {"description":"CPU Utilization (in total and per cpu)", "flag":"-c", "multientry":True},
        "Processors": {"description":"Processors and loads", "flag":"-p", "multientry":False},
        "Processes": {"description":"Processes and threads", "flag":"-P", "multientry":False},
        "Memory": {"description":"Memory- and swap-utilization", "flag":"-m", "multientry":False},
        "Paging": {"description":"Swapping frequency", "flag":"-s", "multientry":False},
        "LVM": {"description":"Logical volumes", "flag":"-l", "multientry":True}, # always has explicit unit for last column
        "Devices": {"description":"Multiple device types", "flag":"-f", "multientry":True},
        "Disks": {"description":"Hard disks", "flag":"-d", "multientry":True}, # always has explicit unit for last column
        "NFS Mounts": {"description":"Information about activity on NFS mounts", "flag":"-n", "multientry":True},
        "NFS Clients": {"description":"Information about NFS client activity", "flag":"-j", "multientry":False},
        "NFS Servers": {"description":"Information about NFS server activity", "flag":"-J", "multientry":False},
        "Network Interfaces": {"description":"Utilization of network interfaces", "flag":"-i", "multientry":True}, # sometimes has explicit unit for last column
        "Network Errors": {"description":"Errors of network interfaces", "flag":"-I", "multientry":True},
        "IPv4": {"description":"IPv4-layer utilization", "flag":"-w", "multientry":False},
        "IPv4 Errors": {"description":"IPv4-layer Errors", "flag":"-W", "multientry":False},
        "IPv6": {"description":"IPv6-layer utilization", "flag":"-z", "multientry":False},
        "IPv6 Errors": {"description":"IPv6-layer Errors", "flag":"-Z", "multientry":False}, # TODO: fix parsing of header (doesn't have a space before _blah_)
        "ICMPv4": {"description":"ICMPv4-layer utilization", "flag":"-y", "multientry":False},
        "ICMPv4 Errors": {"description":"ICMPv4-layer Errors", "flag":"-Y", "multientry":False},
        "ICMPv6": {"description":"ICMPv6-layer utilization", "flag":"-k", "multientry":False},
        "ICMPv6 Errors": {"description":"ICMPv6-layer Errors", "flag":"-K", "multientry":False},
        "UDPv4": {"description":"UDPv4-layer utilization", "flag":"-u", "multientry":False},
        "UDPv6": {"description":"UDPv6-layer utilization", "flag":"-U", "multientry":False},
        "TCP": {"description":"UDPv4-layer utilization", "flag":"-t", "multientry":False},
        "TCP Errors": {"description":"UDPv6-layer utilization", "flag":"-T", "multientry":False},
    }

    def __init__(self, atopsar_binary="atopsar", begin=None, end=None, file=None):
        # collect inputs
        self._atopsar_binary = atopsar_binary
        self._begin = begin
        self._end = end
        self._file = file
        # initialize containers
        self.reports = []

    def get_report(self, begin=None, end=None, file=None, report_type=None):
        # collect inputs
        self._begin = begin
        self._end = end
        self._file = file
        if (report_type is None) or (not report_type in self._report_types.keys()):
            report_type = "CPU"
        report_type_flag = self._report_types[report_type]["flag"]
        # initialize containers
        report_raw = []
        report = []
        report_system = None
        report_date = None
        report_cols = None
        report_groups = set()
        report_data = []
        ismultientry = self._report_types[report_type]["multientry"]
        if ismultientry:
            open_entry = {}
            entry_name = None
            last_time = None
        # build command
        cmd = self._atopsar_binary
        if self._begin is not None:
            cmd += f" -b {self._begin}"
        if self._end is not None:
            cmd += f" -e {self._end}"
        if self._file is not None:
            cmd += f" -r {self._file}"
        cmd += f" {report_type_flag}"
        # launch command
        with subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True) as proc:
            report_raw.append(proc.stdout.read())
        report_raw = report_raw[0].decode("utf-8").split("\n")
        for line in report_raw:
            if (len(line.split()) < 2) or ("logging restarted" in line):
                continue
            if (report_system is None):
                report_system = line
            elif (report_date is None) and ("analysis date" in line):
                line = line.split()
                report_date = line[line.index("date:")+1]
            elif (report_cols is None) and (re.search(".*_\w+_", line)):
                m = re.match(r'.*(_\w+_)', line)
                report_cols = line.replace(m.group(1),'').split()
            else:
                # found new normal entry
                if (not ismultientry) and (len(line.split()) == len(report_cols)):
                    report_data.append(line.split())
                # found new multientry: push previous open entry and collect new one
                elif ismultientry and (
                    (":" in line[:4])
                ):
                    if not open_entry == {}:
                        report_data.append(open_entry)
                    line = line.split()
                    last_time = line[0]
                    entry_name = line[1]
                    report_groups.add(entry_name)
                    if re.search("[^0-9\.]+", line[-1]):  # removes last column if it doesn't look like a number
                        line = line[:-1]
                    open_entry = {
                        "timestamp": last_time,
                        entry_name: line[2:]
                    }
                # found continuing multientry: add to open entry
                elif ismultientry and (
                    (line[:4] == "    ")
                ):
                    line = line.split()
                    entry_name = line[0]
                    report_groups.add(entry_name)
                    if re.search("[^0-9\.]+", line[-1]):  # removes last column if it doesn't look like a number
                        line = line[:-1]
                    open_entry[entry_name] = line[1:]
        report = {
            "system": report_system,
            "date": report_date,
            "cols": report_cols,
            "groups": list(report_groups),
            "data": report_data,
        }
        self.reports.append(report)
        return report


class AtopReader(object):

    def __init__(self, path_schema, atop_binary="atop -f -r {path}"):
        self._atop_binary = atop_binary
        self._path_schema = path_schema

    def required_file_paths(self, begin, end):

        file_name_list = glob.glob(re.sub(r'%[aAwdbBmyYHIpMSfzZjUWcxX]', '*', self._path_schema).replace('**', '*'))
        available_times = sorted(datetime.strptime(file_name, self._path_schema) for file_name in file_name_list)

        for file_index, file_time in enumerate(available_times):
            if file_time < end:
                if len(available_times) > file_index + 1 and available_times[file_index + 1] > begin:
                    # the next item is within the time frame,
                    yield file_time.strftime(self._path_schema)
                elif len(available_times) == file_index + 1:
                    # there is no next item but this is the last item within the time frame
                    yield file_time.strftime(self._path_schema)

    def atop_log_files(self, begin, end):
        for file_path in self.required_file_paths(begin, end):
            yield subprocess.Popen(self._atop_binary.format(path=file_path), stdout=subprocess.PIPE, shell=True).stdout


def main():

    now = datetime.now().replace(second=0, microsecond=0).isoformat()

    ### set up input argument parser
    p = argparse.ArgumentParser()
    subp = p.add_subparsers()
    p.add_argument(
        "-d", "--debug", action='store_true',
        help="whether to add extra print messages to the terminal")
    # main actions
    p_diagram = subp.add_parser("diagram", help="Print the results as a braille character diagram (default).")
    p_diagram.set_defaults(command="diagram")
    p_gnuplot = subp.add_parser("gnuplot", help="Print the results using a gnuplot subprocess.")
    p_gnuplot.set_defaults(command="gnuplot")
    p_table = subp.add_parser("table", help="Print the results as ascii table.")
    p_table.set_defaults(command="table")
    p_csv = subp.add_parser("csv", help="Print the results as csv table.")
    p_csv.set_defaults(command="csv")
    p_json = subp.add_parser("json", help="Print the results as json datagram.")
    p_json.set_defaults(command="json")
    p_list = subp.add_parser("list", help="Print a list of all possible parsable metrics.")
    p_list.set_defaults(command="list")
    # backend commands
    p.add_argument("--cmd", "-c", type=str,
        default="atop -f -r {path}",
        help="Command to call with the raw files. [default: atop -f -r {path}]")
    p.add_argument("--plotstyle", type=str,
        default="dumb",
        help="(gnuplot only) The plot style to use. [default: dumb]")
    p.add_argument("--end", "-e", type=str,
        default=now,
        help="The latest value to plot in ISO8601 format. Defaults to now (%s)." % (now,))
    p.add_argument("--path", "-p", type=str,
        default=r"/var/log/atop/atop_%Y%m%d",
        help="Path to atop raw logs with date placeholders. [default: /var/log/atop/atop_%%Y%%m%%d]")
    p.add_argument("--range", type=int,
        default=6,
        help="Number of hours, backwards from --stop, top plot. [default: 6]")
    p.add_argument("--width", type=int,
        default=59,
        help="Width of plotted graphs in text lines. [default: 59]")
    p.add_argument("--height", "-y", type=int,
        default=9,
        help="Height of plotted graphs in text lines. [default: 9]")
    p.add_argument("--metrics", type=str,
        default="CPL.avg5",
        help="The metric(s) to display (multiple entries must be separated by a comma). [default: CPL.avg5]")
    # parse arguments
    args = p.parse_args()

    time_range = args.range
    metrics = args.metrics.split(",")
    end = iso8601.parse_date(args.end, default_timezone=None)
    begin = end - timedelta(hours=time_range)
    reader = AtopReader(args.path, args.cmd)

    with AtopParser(begin, end) as parser:

        for log_file in reader.atop_log_files(begin, end):
            for line in log_file:
                    parser.add_line(line.decode())

    if not len(parser.result):
        sys.stderr.write('empty result\n')
        sys.exit(1)
    else:
        for m in reversed(metrics):
            if m not in list(parser.available_metric_paths):
                print("WARNING: skipping %s, it was not available (use 'metrics' for a list of options)" % m)
                metrics.pop(metrics.index(m))

    if not "command" in args:
        p.print_help()
    
    elif args.command == "list":

        for metric in parser.available_metric_paths:
            print(metric)

    elif args.command == "table":

        from tabulate import tabulate
        print(tabulate([[time] + [py_.get(value, metric) for metric in metrics]
                        for time, value in parser.result.items()],
                       ['time'] + metrics, tablefmt="plain"))

    elif args.command == "json":

        from json import dumps
        print(dumps({time.isoformat(): {metric: py_.get(value, metric) for metric in metrics}
                     for time, value in parser.result.items()}))

    elif args.command == "csv":

        import csv
        writer = csv.writer(sys.stdout)
        writer.writerow(['time'] + metrics)
        for time, value in parser.result.items():
            writer.writerow([time.isoformat()] + [py_.get(value, metric) for metric in metrics])

    elif args.command == "gnuplot":

        for metric in metrics:

            width = args.width
            height = args.height

            if args.plotstyle == "dumb":
                process = subprocess.Popen(["gnuplot"], stdin=subprocess.PIPE)
            else:
                process = subprocess.Popen(["gnuplot","--persist"], stdin=subprocess.PIPE)
            process.stdin.write(b"set term %b size %d,%d \n" % (args.plotstyle.encode('utf-8'), width, height))
            process.stdin.write(b"unset border \n")
            process.stdin.write(b"unset ytics \n")
            process.stdin.write(b"unset xtics \n")
            process.stdin.write(b"set xtics nomirror \n")
            process.stdin.write(b"unset key \n")

            process.stdin.write(b"set xdata time \n")
            process.stdin.write(b"set format x '%H' \n")
            process.stdin.write(b"set timefmt '%Y-%m-%dT%H:%M:%S' \n")

            process.stdin.write(b"set datafile sep '\t' \n")
            process.stdin.write(b"plot '-' using 1:2 notitle with linespoints \n")

            for time, value in parser.result.items():
                process.stdin.write(b"%s\t%s\n" % (str(time.isoformat()).encode('utf-8'),
                                                   str(py_.get(value, metric)).encode('utf-8')))

            process.stdin.write(b"e\n")
            process.stdin.flush()
            process.stdin.close()
            process.wait()

    elif args.command == "diagram":

        import diagram

        width = args.width
        height = args.height

        class DiagramOptions(object):
            axis = True
            batch = False
            color = False
            encoding = 'utf-8'
            function = None  # None or any of diagram.FUNCTION.keys()
            legend = True
            palette = None  # None or any of diagram.PALETTE.keys()
            reverse = False

            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)

        for metric in metrics:
            engine = diagram.AxisGraph(diagram.Point((width, height)), DiagramOptions())
            engine.update([py_.get(value, metric) for value in parser.result.values()])
            if hasattr(sys.stdout, 'buffer'):
                engine.render(sys.stdout.buffer)
            else:
                engine.render(sys.stdout)


if __name__ == '__main__':
    main()
